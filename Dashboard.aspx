﻿<%@ Page Async="true" Language="C#" AutoEventWireup="true" CodeFile="Dashboard.aspx.cs" Inherits="_Default" %>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="Content/Dashboard.css" media="all">
    <title>Dashboard</title>
  </head>
  <body>
      
      <nav class="navbar navbar-dark bg-dark justify-content-between">
        <a class="navbar-brand">Dashboard</a>
        <form class="form-inline">
            <a id="hyperlinkLogout" href="Login.aspx">Sign Out</a>
        </form>
    </nav>

    <div class="block">
        <div class="row" style="padding: 20px">
            <h3 class="col-12">Overview</h3>
            <div class="row col-12">
                <div class="col-6">
                    <div class="card">
                      <div class="card-header">
                        Card Title
                      </div>
                      <div class="card-body">     
                          <div id="chartDonutDiv" runat="server" style="width: 100%; height: 500px;"></div>
                          <div id="chartDonutScript" runat="server"></div>
                      </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="card">
                      <div class="card-header">
                        Card Title
                      </div>
                      <div class="card-body">      
                          <div id="chartBarDiv" runat="server" style="width: 100%; height: 500px;"></div>      
                          <div id="chartBarScript" runat="server"></div>
                      </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="padding: 20px">
            <h3 class="col-12">User List</h3>
            <div class="row col-12">
                <div class="col-12">
                        <table class="table table-bordered">
                          <thead>
                            <tr>
                              <th scope="col">#</th>
                              <th scope="col">First</th>
                              <th scope="col">Last</th>
                              <th scope="col">Handle</th>
                            </tr>
                          </thead>
                          <tbody id="tblUser" runat="server">
                            <asp:PlaceHolder ID="phTable" runat="server"></asp:PlaceHolder>
                            <%--<tr>
                              <th scope="row">1</th>
                              <td>Mark</td>
                              <td>Otto</td>
                              <td>@mdo</td>
                            </tr>
                            <tr>
                              <th scope="row">2</th>
                              <td>Jacob</td>
                              <td>Thornton</td>
                              <td>@fat</td>
                            </tr>
                            <tr>
                              <th scope="row">3</th>
                              <td>Larry</td>
                              <td>the Bird</td>
                              <td>@twitter</td>
                            </tr>--%>
                          </tbody>
                        </table>
                </div>
            </div>
        </div>
    </div>
      
    <%--<asp:Label ID="lblStatus" runat="server" CssClass="bg-yellow"></asp:Label>--%>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <!-- Resources -->
    <script src="https://www.amcharts.com/lib/4/core.js"></script>
    <script src="https://www.amcharts.com/lib/4/charts.js"></script>
    <script src="https://www.amcharts.com/lib/4/themes/animated.js"></script>
    <!-- Highcharts -->
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>

      <script>
        am4core.ready(function() {

            // DONUT CHART 

            // Themes begin
            am4core.useTheme(am4themes_animated);
            // Themes end

            // Create chart instance
            var chart1 = am4core.create("chartDonutDiv", am4charts.PieChart);

            // Add and configure Series
            var pieSeries = chart1.series.push(new am4charts.PieSeries());
            pieSeries.dataFields.value = "value";
            pieSeries.dataFields.category = "name";

            // Let's cut a hole in our Pie chart the size of 30% the radius
            chart1.innerRadius = am4core.percent(30);

            // Put a thick white border around each Slice
            pieSeries.slices.template.stroke = am4core.color("#fff");
            pieSeries.slices.template.strokeWidth = 2;
            pieSeries.slices.template.strokeOpacity = 1;
            pieSeries.slices.template
              // change the cursor on hover to make it apparent the object can be interacted with
              .cursorOverStyle = [
                {
                  "property": "cursor",
                  "value": "pointer"
                }
              ];

            pieSeries.alignLabels = false;
            pieSeries.labels.template.bent = true;
            pieSeries.labels.template.radius = 3;
            pieSeries.labels.template.padding(0,0,0,0);

            pieSeries.ticks.template.disabled = true;

            // Create a base filter effect (as if it's not there) for the hover to return to
            var shadow = pieSeries.slices.template.filters.push(new am4core.DropShadowFilter);
            shadow.opacity = 0;

            // Create hover state
            var hoverState = pieSeries.slices.template.states.getKey("hover"); // normally we have to create the hover state, in this case it already exists

            // Slightly shift the shadow and make it more prominent on hover
            var hoverShadow = hoverState.filters.push(new am4core.DropShadowFilter);
            hoverShadow.opacity = 0.7;
            hoverShadow.blur = 5;

            // Add a legend
            chart1.legend = new am4charts.Legend();

            chart1.data = dataChart1;
            //    [{
            //  "name": "Lithuania",
            //  "value": 501.9
            //},{
            //  "name": "Germany",
            //  "value": 165.8
            //}, {
            //  "name": "Australia",
            //  "value": 139.9
            //}, {
            //  "name": "Austria",
            //  "value": 128.3
            //}, {
            //  "name": "UK",
            //  "value": 99
            //}, {
            //  "name": "Belgium",
            //  "value": 60
            //    }];


            // LINE CHART
            //var dataChart2 = [{
            //    "name": "USA",
            //    "value": 2025
            //}, {
            //    "name": "China",
            //    "value": 1882
            //}, {
            //    "name": "Japan",
            //    "value": 1809
            //}, {
            //    "name": "Germany",
            //    "value": 1322
            //}, {
            //    "name": "UK",
            //    "value": 1122
            //}, {
            //    "name": "France",
            //    "value": 1114
            //}, {
            //    "name": "India",
            //    "value": 984
            //}, {
            //    "name": "Spain",
            //    "value": 711
            //}, {
            //    "name": "Netherlands",
            //    "value": 665
            //}, {
            //    "name": "Russia",
            //    "value": 580
            //}, {
            //    "name": "South Korea",
            //    "value": 443
            //}, {
            //    "name": "Canada",
            //    "value": 441
            //}, {
            //    "name": "Brazil",
            //    "value": 395
            //}];
            // Create chart instance
            var chart2 = am4core.create("chartBarDiv", am4charts.XYChart);

            // Add data
            chart2.data = dataChart2;

            // Create axes

            var categoryAxis = chart2.xAxes.push(new am4charts.CategoryAxis());
            categoryAxis.dataFields.category = "name";
            categoryAxis.renderer.grid.template.location = 0;
            categoryAxis.renderer.minGridDistance = 30;

            categoryAxis.renderer.labels.template.adapter.add("dy", function (dy, target) {
                if (target.dataItem && target.dataItem.index & 2 == 2) {
                    return dy + 25;
                }
                return dy;
            });

            var valueAxis = chart2.yAxes.push(new am4charts.ValueAxis());

            // Create series
            var series = chart2.series.push(new am4charts.ColumnSeries());
            series.dataFields.valueY = "value";
            series.dataFields.categoryX = "name";
            series.name = "value";
            series.columns.template.tooltipText = "{categoryX}: [bold]{valueY}[/]";
            series.columns.template.fillOpacity = .8;

            var columnTemplate = series.columns.template;
            columnTemplate.strokeWidth = 2;
            columnTemplate.strokeOpacity = 1;

        }); // end am4core.ready()
</script>

  </body>
</html>